USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///Users/touchnote/code/api-test/tests/test-data.csv" AS csvLine 
FIELDTERMINATOR ','
MERGE ( repo:Repository{rid: toInt(csvLine.repo_id), name: csvLine.repo_name } )
MERGE ( user:User { uid: toInt(csvLine.user_id), name: csvLine.user_name  } )
MERGE (user)-[:CONTRIBUTED]->(repo)
MERGE (repo)-[:CONTRIBUTED]->(user)
