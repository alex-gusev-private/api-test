<?php
/*
  The code that follows will make you cry.
  The safety pig is provided for your benefit...
                         _
 _._ _..._ .-',     _.._(`))
'-. `     '  /-._.-'    ',/
   )         \            '.
  / _    _    |             \
 |  a    a    /              |
 \   .-.                     ;  
  '-('' ).-'       ,'       ;
     '-;           |      .'
        \           \    /
        | 7  .__  _.-\   \
        | |  |  ``/  /`  /
       /,_|  |   /,_/   /
          /,_/      '`-' 
*/
/**
 * Created by PhpStorm.
 * User: Alex
 */

namespace Tests\AppBundle\Util;

use AppBundle\Alex\PathFinder;
use AppBundle\Alex\NeoDataProvider;

class PathFinderTest extends \PHPUnit_Framework_TestCase {

    private $client;

    protected function setUp() {
        $this->client = new NeoDataProvider();
    }

    public function testSimple() {
        $pathFinder = new PathFinder($this->client, null);
        $path = [];
        $found = $pathFinder->findPath("Alex", "Mat", $path);
        $hops = count($path) - 1;

        $this->assertEquals(true, $found);
        $this->assertEquals(4, $hops);
    }

    public function testShortestPath() {
        $pathFinder = new PathFinder($this->client, null);
        $path = [];
        $found = $pathFinder->findPath("Alex", "Val", $path);
        $hops = count($path) - 1;

        $this->assertEquals(true, $found);
        $this->assertEquals(4, $hops);
    }

    public function testNoUser() {
        $pathFinder = new PathFinder($this->client, null);
        $path = [];
        $found = $pathFinder->findPath("Gusev", "Val", $path);
        $hops = count($path) - 1;

        $this->assertEquals(false, $found);
        $this->assertEquals(0, $hops);
    }

    public function testNoPath() {
        $pathFinder = new PathFinder($this->client, null);
        $path = [];
        $found = $pathFinder->findPath("Alex", "Kate", $path);
        $hops = count($path) - 1;

        $this->assertEquals(false, $found);
        $this->assertEquals(0, $hops);
    }
}