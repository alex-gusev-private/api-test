<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_120483a9eb97902617c013c97799cfa23adc4f6ed8ad491145a624fdb6c42edd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_749abf635cc5f8981e7ffc6c90345c86bff08ec6e46de297932f2d50a69a9d22 = $this->env->getExtension("native_profiler");
        $__internal_749abf635cc5f8981e7ffc6c90345c86bff08ec6e46de297932f2d50a69a9d22->enter($__internal_749abf635cc5f8981e7ffc6c90345c86bff08ec6e46de297932f2d50a69a9d22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_749abf635cc5f8981e7ffc6c90345c86bff08ec6e46de297932f2d50a69a9d22->leave($__internal_749abf635cc5f8981e7ffc6c90345c86bff08ec6e46de297932f2d50a69a9d22_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a2f46ac47bb7884daf865e6e1911046375756fbccdcb9987930e1a41319e6b3b = $this->env->getExtension("native_profiler");
        $__internal_a2f46ac47bb7884daf865e6e1911046375756fbccdcb9987930e1a41319e6b3b->enter($__internal_a2f46ac47bb7884daf865e6e1911046375756fbccdcb9987930e1a41319e6b3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_a2f46ac47bb7884daf865e6e1911046375756fbccdcb9987930e1a41319e6b3b->leave($__internal_a2f46ac47bb7884daf865e6e1911046375756fbccdcb9987930e1a41319e6b3b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7383cfe2804c6ed3bc59bff6261c85bacc45e9f824847ee54aaefe0cc3991db2 = $this->env->getExtension("native_profiler");
        $__internal_7383cfe2804c6ed3bc59bff6261c85bacc45e9f824847ee54aaefe0cc3991db2->enter($__internal_7383cfe2804c6ed3bc59bff6261c85bacc45e9f824847ee54aaefe0cc3991db2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_7383cfe2804c6ed3bc59bff6261c85bacc45e9f824847ee54aaefe0cc3991db2->leave($__internal_7383cfe2804c6ed3bc59bff6261c85bacc45e9f824847ee54aaefe0cc3991db2_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8e17fe9711cf0799dc9c42f8bd6b24ca25c65dbfd18ef1a3776db730d18638f8 = $this->env->getExtension("native_profiler");
        $__internal_8e17fe9711cf0799dc9c42f8bd6b24ca25c65dbfd18ef1a3776db730d18638f8->enter($__internal_8e17fe9711cf0799dc9c42f8bd6b24ca25c65dbfd18ef1a3776db730d18638f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_8e17fe9711cf0799dc9c42f8bd6b24ca25c65dbfd18ef1a3776db730d18638f8->leave($__internal_8e17fe9711cf0799dc9c42f8bd6b24ca25c65dbfd18ef1a3776db730d18638f8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
