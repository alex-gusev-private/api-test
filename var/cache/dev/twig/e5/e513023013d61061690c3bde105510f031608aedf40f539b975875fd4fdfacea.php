<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_3b26b6d95300136fcc527f26248f7bec9adb127e78c5b55c2a6dff68c93737bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b385cd583864f33d48b3cdcb6283ec1163fdeab2800bb08587245895a183b086 = $this->env->getExtension("native_profiler");
        $__internal_b385cd583864f33d48b3cdcb6283ec1163fdeab2800bb08587245895a183b086->enter($__internal_b385cd583864f33d48b3cdcb6283ec1163fdeab2800bb08587245895a183b086_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b385cd583864f33d48b3cdcb6283ec1163fdeab2800bb08587245895a183b086->leave($__internal_b385cd583864f33d48b3cdcb6283ec1163fdeab2800bb08587245895a183b086_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_64906066356fa8ee758e221a22456106dc6db561aa773df4c1d8e2dca4087fe7 = $this->env->getExtension("native_profiler");
        $__internal_64906066356fa8ee758e221a22456106dc6db561aa773df4c1d8e2dca4087fe7->enter($__internal_64906066356fa8ee758e221a22456106dc6db561aa773df4c1d8e2dca4087fe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_64906066356fa8ee758e221a22456106dc6db561aa773df4c1d8e2dca4087fe7->leave($__internal_64906066356fa8ee758e221a22456106dc6db561aa773df4c1d8e2dca4087fe7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_b8196eb0c33f31d8b0c22c24992e7cbdd396eeca33fe6f10158f88ad89ccbf97 = $this->env->getExtension("native_profiler");
        $__internal_b8196eb0c33f31d8b0c22c24992e7cbdd396eeca33fe6f10158f88ad89ccbf97->enter($__internal_b8196eb0c33f31d8b0c22c24992e7cbdd396eeca33fe6f10158f88ad89ccbf97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_b8196eb0c33f31d8b0c22c24992e7cbdd396eeca33fe6f10158f88ad89ccbf97->leave($__internal_b8196eb0c33f31d8b0c22c24992e7cbdd396eeca33fe6f10158f88ad89ccbf97_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_c0643e5f9c68adcbb44cebe92470267a3cda2a7e538935bd39874577619cd7dc = $this->env->getExtension("native_profiler");
        $__internal_c0643e5f9c68adcbb44cebe92470267a3cda2a7e538935bd39874577619cd7dc->enter($__internal_c0643e5f9c68adcbb44cebe92470267a3cda2a7e538935bd39874577619cd7dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_c0643e5f9c68adcbb44cebe92470267a3cda2a7e538935bd39874577619cd7dc->leave($__internal_c0643e5f9c68adcbb44cebe92470267a3cda2a7e538935bd39874577619cd7dc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
