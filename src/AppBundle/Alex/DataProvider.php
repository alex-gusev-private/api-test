<?php
/*
  The code that follows will make you cry.
  The safety pig is provided for your benefit...
                         _
 _._ _..._ .-',     _.._(`))
'-. `     '  /-._.-'    ',/
   )         \            '.
  / _    _    |             \
 |  a    a    /              |
 \   .-.                     ;  
  '-('' ).-'       ,'       ;
     '-;           |      .'
        \           \    /
        | 7  .__  _.-\   \
        | |  |  ``/  /`  /
       /,_|  |   /,_/   /
          /,_/      '`-' 
*/
/**
 * Created by PhpStorm.
 * User: Alex
 */
namespace AppBundle\Alex;

/**
 * Interface DataProvider
 * Define basic methods to retrieve data.
 *
 * @package AppBundle\Alex
 */
interface DataProvider {
    public function getRepos($userName);
    public function getUsers($repoId, $skipUserName = null);
    public function getRepoName($user1, $user2);
    public function userExists($userName);
}