<?php
/**
 * Created by PhpStorm.
 * User: Alex
 */

namespace AppBundle\Alex;

use Everyman\Neo4j\Client;
use Everyman\Neo4j\Cypher\Query;
use Everyman\Neo4j\Node;
use Everyman\Neo4j\Query\ResultSet;

class Neo4jHelper {

    /**
     * Create a node and set a label if provided.
     *
     * @param Client $client
     * @param array $properties
     * @param $label
     * @return \Everyman\Neo4j\Node
     * @throws \Everyman\Neo4j\Exception
     */
    public static function addNode(Client $client, array $properties, $label) {
        $node = $client->makeNode();
        if (!empty($properties)) {
            foreach ($properties as $key => $value) {
                $node->setProperty($key, $value);
            }
        }
        $node->save();
        if (!empty($label))
        {
            $neoLabel = $client->makeLabel($label);
            $node->addLabels([$neoLabel]);
        }

        /** @noinspection PhpUndefinedClassInspection */
        Log::debug('NODE: '.print_r($node, true));
        return $node;
    }

    /**
     * Delete a node and its relationships.
     *
     * @param Client $client
     * @param array $properties
     * @param $label
     * @return \Everyman\Neo4j\Node
     * @throws \Everyman\Neo4j\Exception
     */
    public static function deleteNode(Client $client, array $properties, $label) {
        $queryString = "MATCH (n:$label)-[r]-() ";
        if (count($properties) > 0) {
            $cnt = 0;
            foreach ($properties as $key => $value) {
                if ($cnt > 0)
                    $queryString .= " AND ";
                $queryString .= "WHERE n." . $key . " = " . $value;
                $cnt++;
            }
        }
        $queryString .= " DELETE n, r";
        $query = new Query($client, $queryString);
        $result = $query->getResultSet();
        /** @noinspection PhpUndefinedClassInspection */
        Log::debug('NODE DELETED: '.print_r($result, true));
        return $result;
    }

    /**
     * Find user's repositories by user.uid.
     *
     * @param Client $client
     * @param $uid
     * @return \Everyman\Neo4j\Query\ResultSet | boolean
     */
    public static function findUserReposByUid(Client $client, $uid) {
        $queryString = "match (user:User)-[:CONTRIBUTED]->(repo:Repository)
                        where user.uid = {uid} return repo order by repo.rid";
        $query = new Query($client, $queryString, ['uid' => $uid]);
        $result = $query->getResultSet();
        /** @noinspection PhpUndefinedClassInspection */
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Find user's repositories by user.name.
     *
     * @param Client $client
     * @param $name
     * @return \Everyman\Neo4j\Query\ResultSet | boolean
     */
    public static function findUserReposByName(Client $client, $name) {
        $queryString = "match (user:User)-[:CONTRIBUTED]->(repo:Repository)
                        where user.name = {name} return repo order by repo.rid";
        $query = new Query($client, $queryString, ['name' => $name]);
        $result = $query->getResultSet();
        /** @noinspection PhpUndefinedClassInspection */
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Find contributors to given repository by repo.rid.
     *
     * @param Client $client
     * @param $rid
     * @return \Everyman\Neo4j\Query\ResultSet | boolean
     */
    public static function findAllContributors(Client $client) {
        $queryString =
                "match (user:User)-[:CONTRIBUTED]->(repo:Repository)
                return user order by user.uid";
        $query = new Query($client, $queryString);
        $result = $query->getResultSet();
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Find contributors to given repository by repo.rid.
     *
     * @param Client $client
     * @param $rid
     * @param null $skipUserName
     * @return bool|ResultSet
     */
    public static function findRepoContributors(Client $client, $rid, $skipUserName = null) {
        if (empty($skipUserName))
            $queryString =
                "match (user:User)-[:CONTRIBUTED]->(repo:Repository) where repo.rid = {rid}
                return user order by user.uid";
        else
            $queryString =
                "match (user:User)-[:CONTRIBUTED]->(repo:Repository)
                where repo.rid = {rid} and user.name != {uname}
                return user order by user.uid";
        $query = new Query($client, $queryString, ["rid" => $rid, "uname" => $skipUserName]);
        $result = $query->getResultSet();
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Find contributors to given repository by repo.rid.
     *
     * @param Client $client
     * @param $user1
     * @param $user2
     * @return bool|ResultSet
     */
    public static function findRepoByContributors(Client $client, $user1, $user2) {
        $queryString =
                "match (user1:User)-[:CONTRIBUTED]->(repo:Repository)<-[:CONTRIBUTED]-(user2:User)
                where user1.name = {name1} and user2.name = {name2}
                return distinct repo order by repo.rid";
        $query = new Query($client, $queryString, ["name1" => $user1, "name2" => $user2]);
        $result = $query->getResultSet();
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Find user's repositories by user.uid.
     *
     * @param Client $client
     * @param $name
     * @return bool|ResultSet
     */
    public static function findUserByName(Client $client, $name) {
        $queryString = "match (user:User)-[:CONTRIBUTED]->() where user.name = {name} return user";
        $query = new Query($client, $queryString, ['name' => $name]);
        $result = $query->getResultSet();
        /** @noinspection PhpUndefinedClassInspection */
        if ($result->count() <= 0) {
            return false;
        }
        return $result;
    }

    /**
     * Convert ResultSet to PHP array.
     *
     * @param $objName
     * @param ResultSet $result
     * @return array
     */
    public static function getAsArray($objName, ResultSet $result) {
        $arr = [];
        foreach ($result as $row) {
            $item = $row[$objName];
            $newItem = Neo4jHelper::getNodeProperties($item);
            $newItem['id'] = $item->getId('id');
            $arr[] = $newItem;
        }
        return $arr;
    }

    /**
     * Return object properties as an array.
     *
     * @param Node $obj - Node Object to get properties from
     * @return array - An array of properties
     */
    public static function getNodeProperties(Node $obj) {
        $ret = [];
        foreach ($obj->getProperties() as $key => $value) {
            $ret[$key] = $value;
        }
        $ret['id'] = $obj->getId();
        return $ret;
    }
}