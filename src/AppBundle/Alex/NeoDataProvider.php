<?php
/*
  The code that follows will make you cry.
  The safety pig is provided for your benefit...
                         _
 _._ _..._ .-',     _.._(`))
'-. `     '  /-._.-'    ',/
   )         \            '.
  / _    _    |             \
 |  a    a    /              |
 \   .-.                     ;  
  '-('' ).-'       ,'       ;
     '-;           |      .'
        \           \    /
        | 7  .__  _.-\   \
        | |  |  ``/  /`  /
       /,_|  |   /,_/   /
          /,_/      '`-' 
*/
/**
 * Created by PhpStorm.
 * User: Alex
 */

namespace AppBundle\Alex;

use Everyman\Neo4j\Client;

/**
 * Class NeoDataProvider
 * Implement Neo4j data provider
 *
 * @package AppBundle\Alex
 */
class NeoDataProvider implements DataProvider {
    /**  @var \Everyman\Neo4j\Client - Neo4J client */
    private $client;

    public function __construct() {
        $this->client = new Client('localhost', 7474);
        $this->client->getTransport()->setAuth('neo4j', 'Curve2016');
    }

    /**
     * Retrieve all repositories for given user
     *
     * @param $userName
     * @return array
     */
    public function getRepos($userName) {
        if (empty($userName))
            return [];
        $resultSet = Neo4jHelper::findUserReposByName($this->client, $userName);
        return empty($resultSet) ? [] : Neo4jHelper::getAsArray("repo", $resultSet);
    }

    /**
     * Retrieve all contributors for given repository.
     *
     * @param $repoId
     * @return array
     */
    public function getUsers($repoId, $skipUserName = null) {
        if (empty($repoId))
            return [];
        $resultSet = Neo4jHelper::findRepoContributors($this->client, $repoId, $skipUserName);
        return empty($resultSet) ? [] : Neo4jHelper::getAsArray("user", $resultSet);
    }

    /**
     * Get repo name that shares 2 users.
     *
     * @param $user1
     * @param $user2
     * @return bool
     */
    public function getRepoName($user1, $user2) {
        if (empty($user1) || empty($user2))
            return false;
        $resultSet = Neo4jHelper::findRepoByContributors($this->client, $user1, $user2);
        $repos = empty($resultSet) ? [] : Neo4jHelper::getAsArray("repo", $resultSet);
        $repo = empty($repos) ? [] : $repos[0];
        return empty($repo) ? false : $repo['name'];
    }

    /**
     * Check if user exists.
     *
     * @param $userName
     * @return bool
     */
    public function userExists($userName) {
        if (empty($userName))
            return false;
        $resultSet = Neo4jHelper::findUserByName($this->client, $userName);
        return empty($resultSet) ? false : true;
    }
}