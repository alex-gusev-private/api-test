<?php
/*
  The code that follows will make you cry.
  The safety pig is provided for your benefit...
                         _
 _._ _..._ .-',     _.._(`))
'-. `     '  /-._.-'    ',/
   )         \            '.
  / _    _    |             \
 |  a    a    /              |
 \   .-.                     ;  
  '-('' ).-'       ,'       ;
     '-;           |      .'
        \           \    /
        | 7  .__  _.-\   \
        | |  |  ``/  /`  /
       /,_|  |   /,_/   /
          /,_/      '`-'
*/
/**
 * Created by PhpStorm.
 * User: Alex
 */

namespace AppBundle\Alex;

/**
 * Class PathFinder
 * @package AppBundle\Alex
 */
class PathFinder {

    /**  @var DataProvider - abstract data provider */
    private $dataProvider;
    /** @var  Monolog instance. Can be null */
    private $logger;
    /** @var array An array for tracking visited users */
    private $visitedUsers;
    /** @var  string - User to search for */
    private $userTo;

    public function __construct(DataProvider $dataProvider, $logger) {
        $this->dataProvider = $dataProvider;
        $this->logger = $logger;
        $this->visitedUsers = [];
    }

    /**
     * Find path from userFrom to userTo.
     *
     * This method adds userFrom to the output array regardless of the search results.
     * Using slightly modified BFS algorithm, it iterates through assumed graph and tracks users first, then adds repos.
     *
     * @param $userFrom - Start user node
     * @param $userTo - End user node
     * @param array $path - A path between users if exists
     * @return bool - True if path was found or false otherwise
     */
    public function findPath($userFrom, $userTo, array &$path) {
        $path[] = $userFrom;
        if (!$this->dataProvider->userExists($userFrom) || !$this->dataProvider->userExists($userTo)) {
            return false;
        }

        $this->userTo = $userTo;

        /** This will be our queue (since it's BFS) */
        $q = [$userFrom];

        $this->visitedUsers = [$userFrom];
        while (count($q) > 0) {
            $currUser = array_shift($q);
            $path[] = $currUser;
            if ($this->logger)
                $this->logger->debug("- USER FROM QUEUE: $currUser");
            $repos = $this->dataProvider->getRepos($currUser);
            /**
             * Beware: the complexity of the following code isn't great, O(r*u), as we have to loop
             * through repos and users.
             * I'd love to reduce it but probably not in this test ;)
             */
            foreach ($repos as $repo) {
                if ($this->logger)
                    $this->logger->debug("<<< PATH: " . print_r($path, true));
                $repoUsers = [];
                if ($this->logger)
                    $this->logger->debug(">>>>>> DOING {$repo['name']}");
                $found = $this->checkRepoUsers($repo, $repoUsers);
                if ($this->logger) $this->logger->debug("   USERS: " . print_r($repoUsers, true));
                if ($found) {
                    $path[] = $this->userTo;
                    break 2;
                } else {
                    if ($this->logger)
                        $this->logger->debug("+ ADDING USERS BACK TO QUEUE");
                    $q = array_merge($q, $repoUsers);
                }
            }
            array_pop($path);
        }

        /**
         * Assemble new array mixing users with repos.
         * At this point in time please help me make less messy...There must be a simpler way to do it.
         */
        $newPath = [];
        $len = count($path);
        if ($this->logger)
            $this->logger->debug("FINAL LEN: $len");
        if ($len > 1) {
            $newPath[] = $userFrom;
            $repoName = $this->dataProvider->getRepoName($userFrom, $path[1]);
            if (empty($repoName)) {
                array_pop($newPath);
            } else {
                $newPath[] = $repoName;
            }
            for ($i = 1; $i < $len - 1; $i++) {
                $user1 = $path[$i];
                $user2 = $path[$i + 1];
                $newPath[] = $user1;
                $repoName = $this->dataProvider->getRepoName($user1, $user2);
                if (!empty($repoName)) {
                    $newPath[] = $repoName;
                }
            }
            $newPath[] = $userTo;
            $found = true;
        } else {
            $newPath = [$userFrom];
            $found = false;
        }
        $path = $newPath;
        return $found;
    }

    /**
     * Check if we have target user among repo contributors.
     *
     * @param array $repo
     * @param $repoUsers
     * @return bool
     */
    protected function checkRepoUsers(array $repo, &$repoUsers) {
        if ($this->logger)
            $this->logger->debug("CURRENT REPO: {$repo['name']}");
        $users = $this->dataProvider->getUsers($repo['rid']);
        if (empty($users)) {
            if ($this->logger)
                $this->logger->debug("CURRENT REPO: {$repo['name']} - NO USERS");
            return false;
        }
        foreach ($users as $user) {
            $userName = $user['name'];
            if (in_array($userName, $this->visitedUsers)) {
                continue;
            }
            $this->visitedUsers[] = $userName;
            if (strcmp($this->userTo, $userName) == 0) {
                if ($this->logger)
                    $this->logger->debug("FOUND!");
                $repoUsers[] = $userName;
                return true;
            }
            $repoUsers[] = $userName;
        }
        return false;
    }
}