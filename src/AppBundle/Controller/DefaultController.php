<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Alex\PathFinder;
use AppBundle\Alex\NeoDataProvider;

class DefaultController extends Controller
{
    /**
     * @var \Everyman\Neo4j\Client - Neo4J client
     */
    private $client;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /**
         * Retrieve request parameters.
         */
        $user1 = $request->get("user1");
        $user2 = $request->get("user2");

        /**
         * Create Neo4j client and set it up. Normally we'd put all connection parameters to some config file
         * but for the sake of this test we hardcode it here.
         */
        $dataProvider = new NeoDataProvider();

        /**
         * Start the search.
         */
        $pathFinder = new PathFinder($dataProvider, $this->get('logger'));
        $path = [];
        $found = $pathFinder->findPath($user1, $user2, $path);
        $hops = count($path) - 1;
        /**
         * Return the path and hops or error. In real life we'd return some error code to support localisation.
         */
        if (empty($found)) {
            echo json_encode(['success' => false, 'hops' => 0, 'error' => "Cannot find path"]);
        } else {
            echo json_encode(['success' => true, 'hops' => $hops, 'path' => $path]);
        }
        exit;
    }
}
