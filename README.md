api-test
========

This is a test project that implements an API endpoint for the shortest path between 2 given contributors for GitHub
repositories. It uses Neo4j graph database to store the test data.

Neo4j installation and running
==============================
Go to neo4j-community-2.2.8 folder and run in Terminal:

bin/neo4j console &

This will start Neo4j engine in console more

Navigating to localhost:7474 in browser to see web interface to the graph db (eg to see the graph or manage data).
You can use the following CYPHER queries to manipulate data:

1. To clean data:
MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r

2. To import the data from CSV file (the path below should be replaced by your real local one):
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///Users/alex/code/api-test/tests/test-data.csv" AS csvLine
FIELDTERMINATOR ','
MERGE ( repo:Repository{rid: toInt(csvLine.repo_id), name: csvLine.repo_name } )
MERGE ( user:User { uid: toInt(csvLine.user_id), name: csvLine.user_name  } )
MERGE (user)-[:CONTRIBUTED]->(repo)

3. To see the data in browser:
MATCH (user:User)-[:CONTRIBUTED]-(repo:Repository) RETURN user, repo

Data file is available in src/tests/test-data.csv

Running the sample
==================
Go to to project root folder and run

php bin/console server:run

This command starts the webserver at http://localhost:8000. To pass paramaters, use the query string like below:

http://localhost:8000/?user1=Alex&user2=Val

The output is a json string with the number of hops and the path if found OR the error message.

Testing
=======
0. There are few Unit tests under /tests/AppBundle/Alex folder
1. No path
curl "http://localhost:8000?user1=Alex&user2=Kate"
2. Search for non-existent users
curl "http://localhost:8000?user1=Alex&user2=Matt"
3. First level search
curl "http://localhost:8000?user1=Alex&user2=Roi"
4. Multiple level search
curl "http://localhost:8000?user1=Mat&user2=Delphine"
curl "http://localhost:8000?user1=Mat&user2=Val"